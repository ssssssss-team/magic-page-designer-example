import { reactive } from 'vue'

// 全局配置，主要用于设置
export default reactive({
  // mpd是否编辑模式
  mpdEditorMode: true
})
