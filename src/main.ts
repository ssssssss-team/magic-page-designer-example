import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'
import MagicPageDesigner from 'magic-page-designer'
// 最后引入自定义css，为了覆盖其它样式
import 'magic-page-designer/dist/index.css'
import './assets/index.less'

const app = createApp(App)

app.use(Antd)
app.use(MagicPageDesigner)
MagicPageDesigner.addAntLibrary()

app.use(router)
app.mount('#app')
