import { createRouter, createWebHashHistory } from 'vue-router'
import { constantRouterMap } from './router.config'

import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const router = createRouter({
  history: createWebHashHistory(),
  routes: constantRouterMap
})

router.beforeEach((to, from, next) => {
  NProgress.start() // start progress bar
  next()
})
router.afterEach(() => {
  setTimeout(function () {
    NProgress.done() // finish progress bar
  }, 400)
})

export default router
