import GlobalView from '../components/layout/GlobalView.vue'
import { RouteRecordRaw } from 'vue-router'
import { HomeOutlined } from '@ant-design/icons-vue'

export const RouteView = {
  name: 'RouteView',
  render: (h: any) => h('router-view')
}

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/dashboard',
    component: GlobalView,
    children: [
      {
        path: 'dashboard',
        name: 'dashboard',
        component: () => import('@/views/Dashboard.vue'),
        meta: {
          icon: HomeOutlined,
          title: '首页'
        }
      }
    ]
  }
]
